import weather_data_funcs

USERNAME = "oliver.silk@powerco.co.nz"
PASSWORD = ""

with open("password.txt", "r") as f:
    PASSWORD = f.readlines()

CURRENT_YEAR = 2022
LOOK_BACK_YEAR = 0

DOWNLOAD_PATH = "Download"
OUTPUT_PATH = "Output"


# Leave blank for all weather stations
WEATHER_STATIONS = (
    "Kaimai"
)



weather_data_funcs.get_weather_stations(
    WEATHER_STATIONS,
    CURRENT_YEAR,
    LOOK_BACK_YEAR,
    USERNAME,
    PASSWORD,
    DOWNLOAD_PATH,
    OUTPUT_PATH,
    False
)